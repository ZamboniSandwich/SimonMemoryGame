package finalproject;
import java.util.Random;
import processing.core.PApplet;
import processing.core.PFont;
public class FinalProject extends PApplet {
	Random random=new Random();
	int simonBox=random.nextInt(4);
	int startTime=0;
	String simonMem =""+simonBox;
	char simonGuess=' ';
	int simonLev=1;
	int index=0;
	int patternIndex=1;
	boolean gameOver=false;
	public void setup() {
		background(255,255,255);;
	      font = createFont("Times New Roman Bold Italic",24,true);
	}
	public void draw() {
	      textFont(font);
	      fill(255,255,255);
			fill(0,0,255);
			rect(300,300,300,300);
			fill(255,0,0);
			rect(300,0,300,300);
			fill(0,153,0);
			rect(0,0,300,300);
			fill(204,204,0);
			rect(0,300,300,300);
		if(simonMem.length()>=patternIndex) {
				if((millis()-startTime)>(1000*patternIndex)) {
					simonBox=Integer.parseInt((""+simonMem.charAt(patternIndex-1)));
					if(simonBox==0) {
						fill(0,200,0);
						rect(100,100,200,200);
					}
					else if(simonBox==1) {
						fill(255,91,91);
						rect(300,100,200,200);
					}
					else if(simonBox==2) {
						fill(249,249,0);
						rect(100,300,200,200);
					}
					else{
						fill(98,98,255);
						rect(300,300,200,200);
					}
					if(millis()-startTime>1000*(patternIndex+1)) {
						fill(0,0,255);
						rect(300,300,300,300);
						fill(255,0,0);
						rect(300,0,300,300);
						fill(0,153,0);
						rect(0,0,300,300);
						fill(204,204,0);
						rect(0,300,300,300);
					}
					if(millis()-startTime>1000*(patternIndex+1)+100)
						patternIndex++;
				}
			}
		if(gameOver) {
			text("GAME OVER. YOUR SCORE WAS "+(simonLev-1)+".",100, 300);
		if(millis()-startTime>3000)
			exit();
		}}
	PFont font;
	public static void main(String[] args) {
		PApplet.main("finalproject.FinalProject");
	}
	public void settings() {
		size(600, 600);
		startTime=millis();
	}
    public void mousePressed() {
    	if(!gameOver) {
			if(mouseX>=0&&mouseX<=300&&mouseY>=0&&mouseY<=300) 
				simonGuess='0';
			else if(mouseX>=300&&mouseX<=600&&mouseY>=0&&mouseY<=300)
				simonGuess='1';
			else if(mouseX>=0&&mouseX<=300&&mouseY>=300&&mouseY<=600)
				simonGuess='2';
			else
				simonGuess='3';
			if(simonGuess==simonMem.charAt(index)&&simonMem.length()-1==index) {
				simonLev++;
				index=0;
				simonBox=random.nextInt(4);
				simonMem=simonMem+simonBox;
				patternIndex=1;
				startTime = millis();
			}
			else if(simonGuess==simonMem.charAt(index))
				index++;
			else {
				gameOver=true;
				startTime = millis();
    	}}}}