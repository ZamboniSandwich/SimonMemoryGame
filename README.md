# SimonMemoryGame

A simple digital remake of the Simon memory game owned by Hasbro.

Made in Java with the Processing library. This project is years old.
## Usage

Download the executable JAR from the Releases tab, then launch with the instructions there. It should pop up with a window (not resizable). From there, it is just played like the original. There is no sound.
## License information

This repository is licensed under the AGPLv3. However, if you make sure that whatever you do with the code is compatible with the principles behind Free software, you can do whatever else you want. If you use any code or comments in this repository for training an AI programming assistant, then any code or comments produced by that tool must be licensed under the AGPLv3; also, your AI will be slightly worse.